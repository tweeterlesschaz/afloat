# Analytics

This directory contains the analytics code for processing the Afloat sensor data.

Please note that there is a Jupyter notebook template for creating new analytic algorithms.
