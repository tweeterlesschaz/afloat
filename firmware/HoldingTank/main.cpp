/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "Libraries/mesh_ack.h"
#include "Libraries/utilities.h"

#include "gitcommit.h"

#include "HoldingTank.h"

#if 0
// Configure debug output. Makes lots of console spew, but helps diagnose Mesh/Cloud connection failures
SerialDebugOutput debugOutput;
#endif


//! Local defines
#define SENSOR_NAME "holding_tank"
#define DATA_FIELD_NAME "level"
#define DESCRIPTION "Monitors the holding tank"

#define SAMPLE_INTERVAL_MS (20/**60*/*1000)
#define CONNECTION_INTERVAL_S 30
#define MAX_NUM_PENDING_ACKS 10

#define WATCHDOG_TIMEOUT_MS (60*1000)


//! Local variables
ApplicationWatchdog softwareWatchDog(WATCHDOG_TIMEOUT_MS, System.reset); // reset the system after 60 seconds if the application is unresponsive

HoldingTank holdingTank(String(SENSOR_NAME),
                        String(DATA_FIELD_NAME),
                        String(DESCRIPTION),
                        SAMPLE_INTERVAL_MS,
                        CONNECTION_INTERVAL_S,
                        MAX_NUM_PENDING_ACKS);

//! Local functions

/**
 * @brief identityHandler Handles messages published to this node
 * @param event The published topic
 * @param byteArrayPacket The published payload
 */
void identityHandler(const char *event, const char *byteArrayPacket)
{
   // DO NOTHING ELSE BUT THE BELOW CALL TO THE SUBCLASSED IDENTITYHANDLER.
   // TODO: FIGURE OUT HOW TO PASS IDENTITYHANDLER AS A FUNCTION POINTER. IT'S MORE OBSCURE THAN YOU THINK.
   holdingTank.identityHandler(event, byteArrayPacket);
}


/**
 * @brief setup Arduino setup routine. Runs once before launching into main().
 */
void setup()
{
   Serial.println("Booting node...");

   // Subscribe to all `whoami`
   Mesh.subscribe(SENSOR_NAME, identityHandler);
   Particle.publishVitals(60);  // Publish vitals every 60 seconds, indefinitely

   // Block until the time is valid
   while (Time.isValid() == false) {
      // Blocking wait. This isn't necessarily the smart thing to do, as the data still
      // has validity even if the time isn't set. However, it will take a fair amount of
      // care to ensure that the system handles an invalid time correctly, and so until
      // that is architected the safest thing to do is block.
   }

   /*
    * Perform any extra startup routines here.
    * .
    * .
    * .
    */

   /* DEBUG output to indicate setup completed */
   Serial.println("Node booted. Entering into main loop...");
}



/**
 * @brief loop Inifinite loop. FreeRTOS equivalent to while(1){}.
 */
void loop()
{
   bool shouldDelaySleep = false;
   bool shouldSleepImmediately = false;

   // Initialize defaults for sleep state output
   static enum SLEEP_STATE sleepState = SLEEP_DONT_SLEEP;

   // Feed the watchdog
   softwareWatchDog.checkin();

   // Run preliminary loop code
   holdingTank.beginLoop(&sleepState, &shouldSleepImmediately);

   // Check if we should jump immediately to sleep
   if (shouldSleepImmediately == true) {
      Serial.println("Going to sleep immediately...");

      goto sleep;
   }


   //================================
   // HANDLE SENSOR EVENTS
   //================================

   // Check if we should get the holding tank sensor measurement
   {
      static time_t nextMeasurementTime_utc = Time.now();
      if (nextMeasurementTime_utc <= Time.now()) {
         static uint32_t count = 0;
         time_t measurementTime = Time.now();
         float tankMeasurement_L = count++;

         // Publish the holding tank values
         holdingTank.publishData(measurementTime, tankMeasurement_L);

         // Update the next measurement time
         while (nextMeasurementTime_utc <= Time.now()) {
            nextMeasurementTime_utc += (uint32_t)(SAMPLE_INTERVAL_MS / 1000.0f + 0.5f);
         }

         // Set the sleep state
         sleepState = SLEEP_TILL_NEXT_SAMPLE;
      }
   }

   //================================
   // DO GENERIC HOUSEKEEPING
   //================================

   // Check the battery
   holdingTank.updateBatteryState();

   // Do network housekeeping
   holdingTank.performNetworkHousekeeping(&sleepState, &shouldDelaySleep);

   // Check if we should delay sleeping
   if (shouldDelaySleep == true) {
      // 1000ms delay to make sure the loop doesn't run too fast. Too many publishes per second overwhelms things
      Serial.println("[MESH] Delaying sleep...");
      delay_nonblocking(1000);
   } else {
sleep:  // GOTO jump point
      switch  (sleepState) {
      case SLEEP_TILL_NEXT_SAMPLE:
      case SLEEP_TILL_INTERRUPTED:
      case SLEEP_TILL_SYNC_TIME:
         holdingTank.handleSleep(sleepState);
         break;
      case SLEEP_UNDETERMINED:
      case SLEEP_DONT_SLEEP:
         // 1000ms delay to make sure the loop doesn't run too fast. Too many publishes per second overwhelms things
         delay_nonblocking(1000);
         break;
      }
   }
}
