# Firmware

This project uses a Nexus (Particle Boron or Argon) as a central point and communicates with various Nodes (Particle Xenon). The Particle ecosystem connects the Xenon/Argon/Boron platforms via a 802.15.4 mesh network. 

Therefore, the hardware has to be on the same mesh network (the details of doing this are outside the scope of the Readme. Please refer to particle.io).

The code is compiled with Particle's CLI. Please note that this project uses Qt Creator as an IDE, instead of Particle's preferred Workbench (a plugin living on top of Visual Studio). This makes no difference, except that without a Visual Studio project file the code cannot be compiled inside of Workbench.

To compile, simply use the makefile:


```
# Make the program
make build APP_TARGET=BilgeSensor
```

The code currently is written for a nexus, a bilge sensor, and a holding tank sensor. This will be extended as new sensors nodes are brought online.

Please see more extensive documentation at https://docs.google.com/document/d/1D6Pq7KHzkSfK1Q7eN6TOOyOH8XxJank5xZNvTEH25CA/edit?usp=sharing
