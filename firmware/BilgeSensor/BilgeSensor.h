#ifndef HOLDING_TANK_H
#define HOLDING_TANK_H

#include "Libraries/AfloatAbstractNode.h"

class BilgeSensor : public AfloatAbstractNode {
public:
   BilgeSensor(String sensorName, String dataFieldName, String description, std::initializer_list<pin_t> wakeUpPins, std::initializer_list<InterruptMode>, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks);

   void identityHandler(const char *event, const char *jsonMsgPacket);
   void publishData(time_t cycleTime, float cycleCount, uint32_t numberOfRetries = 0);
   void republishData(const payload_t &payloadData, uint32_t numberOfRetries = 0);

};



#endif  // HOLDING_TANK_H
