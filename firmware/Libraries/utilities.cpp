/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "utilities.h"


/**
 * @brief delay_nonblocking Delays without blocking Particle subsystem routines
 * @param delay_ms Time [in ms] to delay
 */
void delay_nonblocking(uint32_t delay_ms)
{
   uint32_t oldTime = millis();
   while (millis() - oldTime < delay_ms) {
      Particle.process();
   }
}


