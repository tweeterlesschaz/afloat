#ifndef MESH_ACK_H
#define MESH_ACK_H


typedef struct __attribute__((packed)) {
      uint8_t ID;  // Acknowledgement ID
      const char data[];  // Data being sent to Mesh
} MeshPacketWithAck_t;

class MeshPacketWithAck {
public:
   MeshPacketWithAck(uint32_t size) {
      packet = (MeshPacketWithAck_t *)malloc(size);
      this->size = size;
   }
   ~MeshPacketWithAck() {
      free(packet);
   }

   MeshPacketWithAck_t *packet;
   uint32_t size;
};

class CircularBufferWithForgetting {

public:

   CircularBufferWithForgetting(uint32_t numBufferRows);
   ~CircularBufferWithForgetting();

   void enqueue(int newVal);
   void dequeue();
   int32_t remove(int32_t ID);

   uint32_t countStillActive;

   uint32_t numBufferRows;

   uint32_t head = 0;
   uint32_t tail = 0;

private:
   int16_t *data;

   void compactBuffer();
};


int32_t publishWithAck(const char *name, MeshPacketWithAck &packet, uint8_t *packetID = nullptr);
int32_t receiveAck(int32_t ID);
int32_t haltAck(int32_t ID);

#endif  // MESH_ACK_H
